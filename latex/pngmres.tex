\documentclass[10pt]{article}

\input{preamble.tex}

\begin{document}

% actual intro: global communication = bottleneck
% pipelined algorithms = hide global communication

In this blurb, we provide an overview of a nonlinear Krylov acceleration technique described by Washio and Oosterlee and details of its implementation in the scientific computing software PETSc. Then we recommend pipelining strategies that might reduce the cost of the algorithm and hide global communication phases.

Let $F$ be a nonlinear system and $M$ a solution method. In the original text, $M$ was a nonlinear multigrid technique, but PETSc allows for any right preconditioner or, in the absence of one, a line search method. The algorithm builds a set of intermediate solutions and terminates when the residual error is below a tolerance or some number of iterations is reached. The following are applied in each iteration.
\begin{enumerate}
\item Compute a new solution $u^M$ using $M$, $F$, and previous solution.
\item Compute updated solution $u^A$ using $u^M$ and Krylov subspace.
\item Select either $u^M$ or $u^A$ as the new solution and determine whether or not to restart method.
\end{enumerate}


We will use mathematical notation to describe the algorithm and describe the implementation in the PETSc SNES solver NGMRES. 

For now, assume that $k<m$ where $m$ is a predetermined number of storage vectors. In iteration $k$ we have at most $m$ intermediate solutions
\begin{equation}
u_0, u_1, \dots u_{k-1} 
\label{eq:solutions}
\end{equation}
stored in a vector and intermediate residuals
\begin{equation}
F(u_0), F(u_1), \dots F(u_{k-1})
\label{eq:residuals}
\end{equation}
also stored in a vector of length $m$. PETSc also stores the residual norms 
\begin{equation}
\norm{F(u_0)}_2, \norm{F(u_1)}_2, \dots \norm{F(u_{k-1})}_2
\label{eq:residualnorms}
\end{equation}
and keeps track of the smallest norm in \texttt{fminnorm}.

\bigskip

\noindent Compute $u^M$:
Using $M$ and $F$, compute $u^M$, its residual, and residual norm 
\begin{equation}
u^M = M(F, u_{k-1}), \quad F(u^M), \quad (F(u^M), F(u^M)).
\label{eq:uM}
\end{equation}
If no preconditioner $M$ is provided, PETSc performs gradient descent with line search in the direction of $F$ to compute $u^M$.
The residual norm $\norm{F(u^M)}$ is computed here, too.

\bigskip

\noindent Compute $u^A$:
Look for $u^A$ in 
\begin{equation}
u^A \in u^M + \text{span}\{u_0 - u^M, u_1 - u^M, \dots, u_{k-1} - u^M\}. \label{eq:subspace}
\end{equation}
The vector space in \eqref{eq:subspace} is related to the Krylov subspace $K(FM^{-1}, F(u_0))$. To find $u^A$,  linearize $F$ and solve the minimization problem 
\begin{equation}
\min \norm{F(u^M) + \sum_{i = 1}^k \alpha_i (F(u_{i-1}) - F(u^M))}_2
\label{eq:minimization}
\end{equation}
with respect to the coefficients $\alpha_i$, $1 \leq i \leq k$.
In practice, \eqref{eq:minimization} can be solved by solving a linear least squares problem $H \alpha = \beta$ for $\alpha$.
The $k \times k$ matrix $H$ has elements
\begin{multline}
h_{i, j} = (F(u_{i-1}), F(u_{j-1})) - (F(u^M), F(u_{i-1})) \\ - (F(u^M), F(u_{j-1})) + (F(u^M), F(u^M)) \quad \quad 
\label{eq:h}
\end{multline}
and the vector $\beta$ has $k$ elements 
\begin{equation}
\beta_i = (F(u^M), F(u^M)) - (F(u^M), F(u_{i-1})), \quad 1 \leq i \leq k.
\label{eq:beta}
\end{equation}
The inner product $(F(u^M), F(u^M))$ has already been computed
The inner products of $F(u^M)$ and all previous residuals $F(u_0), \dots,F(u_{k-1})$,
\begin{equation}
\xi_i = (F(u^M), F(u_{i-1})), \quad 1 \leq i \leq k,
\label{eq:xi}
\end{equation} 
need to be computed. So do the inner product of the last residual $F(u_{k-1})$ and 
all previous residuals $F(u_0), \dots,F(u_{k-1})$
\begin{align}
q_{k, i} &= (F(u_{k-1}), F(u_{i-1}))  \label{eq:q} \\
q_{i, k} &= q_{k, i}, \quad 1 \leq i \leq k \nonumber
\end{align}
Then $h_{i, j} = q_{i, j} - \xi_i - \xi_j + (F(u^M), F(u^M))$ and $\beta_i = (F(u^M), F(u^M)) - \xi_i$.

The PETSc implementation performs the inner products \eqref{eq:xi} and \eqref{eq:q} using the function \texttt{VecMDotBegin/End} before assembling the matrix $H$ and vector $\beta$.
The inner products in \eqref{eq:q} can be stored in a matrix $Q$ and accessed in subsequent iterations.

Here, the original authors and PETSc implementations diverge. Washio and Oosterlee note that the matrix $H$ can be singular so that the vector $\alpha$ is not necessarily unique. 
They advocate  solving $(H + \delta I) \alpha = \beta$ where $\delta = \epsilon \max(h_{1, 1}, \dots, h_{k, k})$ and $\epsilon = 10^{-16}$. They show that this introduces a negligible error to one of the solutions in the original system. PETSc, on the other hand, calls the LAPACK routine \texttt{gelss} to solve the linear least squares problem $H \alpha = \beta$ for $\alpha$. 

Now compute the updated solution $u^A$ with coefficients
\begin{equation}
u^A = u^M + \sum_{i = 1}^k \alpha_i (u_{k-i} - u^M).
\label{eq:uA}
\end{equation}

\bigskip

\noindent Select $u^A$ or $u^M$ as the new solution: We keep the updated solution $u^A$ if the residual norm of $u^A$ is not big compared to $u^M$ or intermediate solutions 
\begin{equation}
\norm{F(u^A)}_2 < \gamma_A \min(\norm{F(u^M)}_2, \norm{F(u_0)}_2, \dots, \norm{F(u_{k-1})}_2)
\label{eq:select1}
\end{equation}
and the updated solution is not too close to other solutions unless the residual norm is an improvement
\begin{equation}
\epsilon_B \norm{u^A - u^M}_2 < \min(\norm{u^A - u_0}_2, \norm{u^A - u_1}_2, \dots, \norm{u^A - u_{k-1}}_2)
\label{eq:select2}
\end{equation}
\begin{equation}
\text{ or } \norm{F(u^A)}_2 < \delta_B \min(\norm{F(u^M)}_2, \norm{F(u_0)}_2, \dots, \norm{F(u_{k-1})}_2)
\label{eq:select3}
\end{equation}
for $\gamma_A$, $\epsilon_B$, and $\delta_B$. In PETSc, the constants have default values $\gamma_A = 2.0$, $\epsilon_B = 0.1$, and $\delta_B = 0.9$ proposed in the original work. They can also be defined by the user.

To check \eqref{eq:select1}, \eqref{eq:select2}, and \eqref{eq:select3}, we need to compute the residual norm of $u^A$ and calculate the distance between  $u^A$ and $u^M$ and $u^A$ and the previous solutions $u_0, \dots, u_{k-1}$
\begin{equation}
\norm{F(u^A)}_2, \norm{u^A - u^M}_2, \norm{u^A - u_0}_2, \dots, \norm{u^A - u_{k-1}}_2.
\end{equation}
The norms on the right hand side of \eqref{eq:select1} and \eqref{eq:select3} have already been computed. 

If $u^A$ is selected, update the intermediate solutions and residuals with 
\begin{equation}
u_k = u^A, \quad F(u_k) = F(u^A)
\end{equation}
and add to the lists \eqref{eq:solutions} and \eqref{eq:residuals}.
Similarly, if $u^M$ is selected, update the intermediate solutions and residuals with
\begin{equation}
u_k = u^M, \quad F(u_k) = F(u^M).
\end{equation}

\bigskip

\noindent Decide whether or not to restart: The algorithm restarts if one of the following two conditions are true in two consecutive iterations. PETSc allows for a variable number of consecutive iterations.
\begin{equation}
\norm{F(u^A)}_2 \geq \gamma_C \min(\norm{F(u^M)}_2, \norm{F(u_0)}_2, \dots, \norm{F(u_{k-1})}_2)
\label{eq:restart1}
\end{equation}
\begin{equation}
\epsilon_B \norm{u^A - u^M}_2 \geq \min(\norm{u^A - u_0}_2, \norm{u^A - u_1}_2, \dots, \norm{u^A - u_{k-1}}_2)
\label{eq:restart2}
\end{equation}
\begin{equation}
\text{ and } \norm{F(u^A)}_2 < \delta_B \min(\norm{F(u^M)}_2, \norm{F(u_0)}_2, \dots, \norm{F(u_{k-1})}_2)
\label{eq:restart3}
\end{equation}
for $\gamma_C$,  $\epsilon_B$, and $\delta_B$. The constants $\epsilon_B$ and $\delta_B$ are the same as before, and $\gamma_C$ has a PETSc default $\gamma_C = 2.0$. PETSc includes the option to restart whenever the residual $F(u^M)$ increases, flagged by \texttt{restart\_fm\_rise}. The restart inequalities \eqref{eq:restart1}, \eqref{eq:restart2}, \eqref{eq:restart3} are the opposite of those that select the new solution \eqref{eq:select1}, \eqref{eq:select2}, \eqref{eq:select3} so that no global work is done here. 

\bigskip

When the iteration $k = m$ and beyond, the stored vectors and values begin to be replaced, beginning with the first, e.g.
\begin{equation}
u_m, u_1, \dots u_{k-1}. 
\end{equation}
Then the minimization problem above has $m$ unknowns and the indicies in \eqref{eq:h}, \eqref{eq:beta}, \eqref{eq:xi} run from $1 \leq i \leq m$ instead of up to $k$.
The matrix $Q$ update \eqref{eq:q} becomes 
\begin{align}
q_{j, i} &= (F(u_{j-1}), F(u_{i-1})) \\
q_{i, j} &= q_{j, i}, \quad 1 \leq i \leq m \nonumber
\end{align}
with $j =\text{mod}(k, m)$. The ideas remains the same.

\bigskip

Now we will discuss possible opportunities for pipeling. 
Pipelining strategies include loop unrolling and arithmetic rearrangements, perhaps at the cost of some additional work and storage.

\begin{itemize}
\item The inner products \eqref{eq:q} can be performed anytime after the solution $u_{k-1}$ is chosen in the previous iteration. The original authors wrote the algorithm this way. 
\item The vector subtraction in \eqref{eq:uA} could be computed earlier, although this is a small amount of computation. 
\item There is perhaps potential in implementing a  linear solver that begins with missing information or incrementally solving the least squares problem as the inner products \eqref{eq:xi} are computed.
\end{itemize}

\end{document}
